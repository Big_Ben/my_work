let carts = document.querySelectorAll('.add-cart');
let products = [
    {
        name:'All the piano you need',
        tag:'Mireille Dumas',
        rating:4.1,
        price: 12,
        inCart:0
    },
    {
        name:'Songs to test by',
        tag:'Francis Quokka',
        rating:4.9,
        price: 14,
        inCart:0
    },
    {
        name:'The Greatest Show on Earth',
        tag:'Jean-Luc Kekes',
        rating:4.3,
        price: 16.50,
        inCart:0
    },
    {
        name:'Dream is collapsing',
        tag:'Emy Le Iench',
        rating:4.6,
        price: 14.50,
        inCart:0
    },
    {
        name:'Furious Ferrieu',
        tag:'Yanis Bensetti',
        rating:4.2,
        price: 12.50,
        inCart:0
    },
    {
        name:'Mr. Roquefort',
        tag:'Océane and the Queens',
        rating:4.8,
        price: 14,
        inCart:0
    }
];

for( let i=0; i< carts.length; i++){
    carts[i].addEventListener('click', ()=>{
        cartNumbers(products[i]);
        totalCost(products[i])
    })
}

function onLoadCartNumbers(){
    let productNumbers = localStorage.getItem('cartNumbers');

    if(productNumbers){
        document.querySelector('.cart span').textContent = productNumbers;
    }
}

function cartNumbers(product){
    
    let productNumbers = localStorage.getItem('cartNumbers');


    productNumbers = parseInt(productNumbers);

    if(productNumbers){
        localStorage.setItem('cartNumbers',productNumbers+1);
        document.querySelector('.cart span').textContent = productNumbers+1;
    }else{
        localStorage.setItem('cartNumbers',1);
        document.querySelector('.cart span').textContent = 1;
    }

    setItems(product);

}

function setItems(product){
    let cartItems = localStorage.getItem('productsInCart');
    cartItems = JSON.parse(cartItems);
    if(cartItems != null){
        if(cartItems[product.tag] == undefined){
            cartItems={
                ...cartItems,
                [product.tag]: product
            }
        }
        console.log(cartItems[product.tag]);
        cartItems[product.tag].inCart +=1;
    }else{
        product.inCart = 1;

        cartItems = {
            [product.tag]: product
    }
    }    
    localStorage.setItem("productsInCart",JSON.stringify(cartItems));

}

function totalCost(product){
    // console.log("the product price is ", product.price);
    let cartCost = localStorage.getItem('totalCost');

    console.log("My cartCost is", cartCost + product.price);
   console.log(typeof cartCost);

    if (cartCost != null) {
        cartCost = parseInt(cartCost);
        localStorage.setItem("totalCost", cartCost + product.price);
    }else{
        localStorage.setItem("totalCost", product.price);
    }
}

function displayCart(){
    let cartItems = localStorage.getItem('productsInCart');
    cartItems = JSON.parse(cartItems);
    console.log(cartItems);
    let productContainer = document.querySelector(".products");
    let cartCost = localStorage.getItem('totalCost');
    
    console.log(cartItems);
    if (cartItems && productContainer) {
        productContainer.innerHTML = '';
        Object.values(cartItems).map(item => {
            productContainer.innerHTML += `
            <div class = "product">
                <span>${item.name}</sapn>
            </div>
            <div class="price">$${item.price}</div>
            <div class="quantity">
                <span>${item.inCart}</span>
            </div>
            <div class="total">
                $${item.inCart * item.price}
            </div>
            `;
        
        });

        productContainer.innerHTML += `
            <div class="basketTotalContainer">
                <h4 class="basketTotalTitle">
                    Total
                </h4>
                <h4 class ="basketTotal">
                    $${cartCost}
                </h4>
        `;
    }
}

onLoadCartNumbers();
displayCart();